var fs = require('fs');
var path = require('path');
var errToConsole = true;
var pathConfigs = {
  '/': {
    view: 'index',
    title: 'Index',
    inlineStyles: getFileContents(['/styles/core.css']),
    remoteStyles: ['https://fonts.googleapis.com/css?family=Roboto:' +
      '400,300,700,500,400italic'],
    remoteScripts: ['/scripts/static-page.js']
  },
  '/projectidea': {
    view: 'projectidea',
    title: 'Project Idea',
    inlineStyles: getFileContents(['/styles/core.css']),
    remoteStyles: ['https://fonts.googleapis.com/css?family=Roboto:' +
    '400,300,700,500,400italic'],
    remoteScripts: ['/scripts/static-page.js']
  },
  '/formingacoreteam': {
    view: 'formingacoreteam',
    title: 'forming a core team',
    inlineStyles: getFileContents(['/styles/core.css']),
    remoteStyles: ['https://fonts.googleapis.com/css?family=Roboto:' +
      '400,300,700,500,400italic'],
    remoteScripts: ['/scripts/static-page.js']
  },
  '/waystoorganiseateam': {
    view: 'waystoorganiseateam',
    title: 'ways to organise a team',
    inlineStyles: getFileContents(['/styles/core.css']),
    remoteStyles: ['https://fonts.googleapis.com/css?family=Roboto:' +
      '400,300,700,500,400italic'],
    remoteScripts: ['/scripts/static-page.js']
  },
  '/location': {
    view: 'location',
    title: 'location',
    inlineStyles: getFileContents(['/styles/core.css']),
    remoteStyles: ['https://fonts.googleapis.com/css?family=Roboto:' +
      '400,300,700,500,400italic'],
    remoteScripts: ['/scripts/static-page.js']
  },
  '/supports': {
    view: 'supports',
    title: 'supports',
    inlineStyles: getFileContents(['/styles/core.css']),
    remoteStyles: ['https://fonts.googleapis.com/css?family=Roboto:' +
      '400,300,700,500,400italic'],
    remoteScripts: ['/scripts/static-page.js']
  },
  '/buildingtheproposal': {
    view: 'buildingtheproposal',
    title: 'building the proposal',
    inlineStyles: getFileContents(['/styles/core.css']),
    remoteStyles: ['https://fonts.googleapis.com/css?family=Roboto:' +
      '400,300,700,500,400italic'],
    remoteScripts: ['/scripts/static-page.js']
  },
  '/criticalmassofsupport': {
    view: 'criticalmassofsupport',
    title: 'building a critical mass of support',
    inlineStyles: getFileContents(['/styles/core.css']),
    remoteStyles: ['https://fonts.googleapis.com/css?family=Roboto:' +
      '400,300,700,500,400italic'],
    remoteScripts: ['/scripts/static-page.js']
  },
  '/projectgoaheadagreed': {
    view: 'projectgoaheadagreed',
    title: 'project go ahead agreed',
    inlineStyles: getFileContents(['/styles/core.css']),
    remoteStyles: ['https://fonts.googleapis.com/css?family=Roboto:' +
      '400,300,700,500,400italic'],
    remoteScripts: ['/scripts/static-page.js']
  },
  '/afterbuild': {
    view: 'afterbuild',
    title: 'after build',
    inlineStyles: getFileContents(['/styles/core.css']),
    remoteStyles: ['https://fonts.googleapis.com/css?family=Roboto:' +
      '400,300,700,500,400italic'],
    remoteScripts: ['/scripts/static-page.js']
  },
  '/compass': {
    view: '',
    title: 'Compass Planner',
    inlineStyles: getFileContents(['/styles/core.css']),
    remoteStyles: ['https://fonts.googleapis.com/css?family=Roboto:' +
      '400,300,700,500,400italic'],
    remoteScripts: ['/scripts/core.js']
  }
};

function getFileContents(files) {
  // Concat inline styles for document <head>
  var flattenedContents = '';
  var pathPrefix = '/../../dist/';
  files.forEach(function(file) {
    flattenedContents += fs.readFileSync(path.resolve(__dirname) +
      pathPrefix + file);
  });

  return flattenedContents;
}

module.exports = {
  getConfig: function(urlPath) {
    var object = pathConfigs[urlPath];

    // Check if the path is actually valid.
    if (!object) {
      return null;
    }

    return {
      'data': object
    };
  }
};
